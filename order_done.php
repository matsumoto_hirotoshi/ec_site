<?php
session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');

include "functions.php";
include "urls.php";

$pdo = initDB();
try {
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    $pdo->beginTransaction();
    $pdo->exec("INSERT INTO orders (`user_id`, `item_id`, `num`)
          SELECT charts.`user_id`, charts.`item_id`, charts.`num`
          FROM charts
          WHERE charts.`user_id` = '{$_SESSION["userId"]}';");
    $pdo->exec("DELETE FROM charts WHERE user_id = '{$_SESSION["userId"]}'");
    $pdo->commit();
} catch (Exception $e) {
    $pdo->rollBack();
    echo "失敗しました。" . $e->getMessage();
}

header("Location: {$top_page}");
