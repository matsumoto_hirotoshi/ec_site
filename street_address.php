<?PHP
session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');

include "load_smarty.php";
include "urls.php";
include "functions.php";
include "qeries.php";

//登録済みの住所を取得
$result = getDestinations($DESTINATIONS_QUERY);
$smarty->assign('result', $result);

//都道府県を取得
$prefectures = getPrefectures();
$smarty->assign('prefectures', $prefectures);


$smarty->display('street_address.tpl');
