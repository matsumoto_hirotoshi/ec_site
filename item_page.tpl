<!DOCTYPE html>
<html>
	<head>
	 <meta http-equiv="Content-Type" 
            content="text/html; charset=UTF-8">
		<title>ec_site</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
		function chartButtonOnClick(obj){
			var num = document.getElementsByName("num")[0].value;
			window.location.href = 'shop_chart.php?&num=' + num;
		}
		</script>
		<body>
			<header>
				<div class="topForm">
					<form id="search_form" action="drag_store.php" method="post">
						<p><a href="{$top_page}"><img src="image/tengoku.png"></a>カテゴリ
							<select name="category" method="post">
								<option value="NULL">すべて</option>
								{foreach from=$categoryArray item=value key=key}
									<option value={$value}>{$key}</option>
								{/foreach}
							</select>

							キーワードで調べる<input type="text" size="12" name="itemName" value="{$itemName}"/>
							<input type="submit" name="submit" value="絞込検索">

							{if $userSession == true}
								ようこそ<a href="{$order_history_page}">{$userName}</a>さん
								<a href="{$shop_chart_page}">ショッピングカートへ</a>
								<a href="{$logout_page}">ログアウト</a>する
							{else}
								<a href="{$login_page}">ログイン</a>する
								<a href="{$register_page}">新規会員登録</a>
							{/if}
					</form>
				<div>
			</header>
			<div class="warapper">
				<div class="leftCont">
					<div class="userMenu">
						<ul style="list-style:none;">
							{if $userSession == true}
							<li><a href="{$shop_chart_page}">カートを見る</a></li>
							<li><a href="{$order_history_page}">購入履歴</a></li>
							{/if}
						</ul>
					</div>
					<div class="categoryMenu">
						<ul style="list-style:none;">
							<li>カテゴリ</li>
							{foreach from=$categoryArray item=value key=key}
	        					<li><a href="{$top_page}?category={$value}">{$key}</a></li>
	        				{/foreach}
	    				</ul>
					</div>
				</div>
				<div class="rightCont">
					{foreach from=$itemResult item=value key=key}
						<img src=image/{$value["image_path"]} width="150" height="150" alt="画像"></a><br>
						{$value["item_name"]}<br>
						<a href="{$top_page}?brand={$value["brand"]}">{$value["brand"]}</a><br>
						内容物<br><div class="informationText">{$value["ingredients"]}</div><br>
						商品の説明<br><div class="informationText">{$value["information"]}</div>
					{/foreach}
					<p>
						<tr>
							<td>
								<span style="color:red;font-weight: bold;">&yen; {$price}</span> 個数：<select name="num">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>
								
								<input type="button" onclick="chartButtonOnClick(this.form)"value="カートに入れる">
							</td>
						</tr>
					</p>
				</div>
			</div>
		</body>
	</head>
<html>
