<?php
// エラーが発生した場合にエラー表示をする設定
ini_set('display_errors', 1);

// Smartyを使うための準備
require_once('/home/www/smarty-3.1.34/libs/' . 'Smarty.class.php');

$smarty = new Smarty();

$smarty->compile_dir  = '/home/www/smarty-3.1.34/templates_c/';
$smarty->cache_dir    = '/home/www/smarty-3.1.34/cache/';
