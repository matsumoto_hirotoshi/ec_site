<!DOCTYPE html>
<html>
	<head>
	 <meta http-equiv="Content-Type"
            content="text/html; charset=UTF-8">
		<title>ec_site</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
		<script type="text/javascript" src="script/chartCalc.js"></script>
		<body>
			<header>
				<div class="topForm">
					<form id="search_form" action="drag_store.php" method="post">
						<p><a href="{$top_page}"><img src="image/tengoku.png"></a>カテゴリ
							<select name="category" method="post">
								<option value="NULL">すべて</option>
								{foreach from=$categoryArray item=value key=key}
									<option value={$value}>{$key}</option>
								{/foreach}
							</select>

							キーワードで調べる<input type="text" size="12" name="itemName" value="{$itemName}"/>
							<input type="submit" name="submit" value="絞込検索">

							{if $userSession == true}
								ようこそ<a href="{$order_history_page}">{$userName}</a>さん
								<a href="{$shop_chart_page}">ショッピングカートへ</a>
								<a href="{$logout_page}">ログアウト</a>する
							{else}
								<a href="{$login_page}">ログイン</a>する
								<a href="{$register_page}">新規会員登録</a>
							{/if}
					</form>
				<div>
			</header>
			<div class="centerFont">ショッピングカート</div>
			<div class="inChartItem">
				<table class="costTable">
					<tr>
						<td>商品名</td>
						<td>単価(税込)</td>
						<td>注文個数</td>
						<td>削除</td>
					</tr>

					{foreach from=$itemResult item=value key=key}
						<tr>
							<td class="item_id{$value['item_id']}" width="450">
							<a href="{$item_page}?id={$value['item_id']}">{$value['item_name']}</a></td>
							<td class="cost">{$value["price"]}円</td>

							<td>
								<select class="itemNum">
									{$value["numberElem"]}
								</select>
							</td>

							<td><span><input type="image" src="image/button/btn_delate.gif" name="delete_button"></span></td>
						</tr>
					{/foreach}

				</table>
			</div>
			<div class="totalCost">
			合計金額：<span id="totalCost"style="color:red;font-weight: bold ;">{$total}円</span>
			</div>
			<div align="right"><a href="{$street_address_page}"><img src="image/button/btn_kounyu_red.png" alt="button"></a>
			</div>
		</body>
	</head>
<html>
