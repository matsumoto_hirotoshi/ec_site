<!DOCTYPE html>
<html>
	<head>
	 <meta http-equiv="Content-Type" 
            content="text/html; charset=UTF-8">
		<title>ec_site</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<body>
			<center>
				<h1>注文内容</h1>
				<table class="registerAddresTable">
					<tr>
						<td>宛先</td>
						<td>{$destination["post_name"]}</td>
					</tr>
					<tr>
						<td>郵便番号</td>
						<td>{$destination["postcode"]}</td>
					</tr>
					<tr>
						<td>住所</td>
						<td>{$destination["address"]}</td>
					</tr>
					<tr>
						<td>電話番号</td>
						<td>{$destination["phone_number"]}</td>
					</tr>
				</table>
			</center>
			<br clear="left">
			<div class="orderItem">
				<table class="costTable">
					<tr>
						<td>商品名</td>
						<td>単価</td>
						<td>個数</td>
					</tr>

					{foreach from=$charts item=value key=key}
						<tr>
							<td>{$value['item_name']}</td>
							<td>{$value['price']}</td>
							<td>{$value['num']}</td>
						</tr>
					{/foreach}

				</table>
			</div>

			<div class="totalCost">
			合計金額：<span id="test"style="color:red;font-weight: bold ;">{$chartTotalPrice}円</span>
			</div>

			<form id="search_form" action="order_done.php" method="post">
				<center><button type="submit" value="chart">購入する</button></center>
			</form>
		</body>

	</head>
<html>