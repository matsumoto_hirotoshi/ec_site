<?php
@session_start();
$DESTINATIONS_QUERY =
"SELECT * FROM destinations
 LEFT JOIN prefectures ON destinations.prefecture_id = prefectures.prefecture_id
 WHERE user_id = {$_SESSION['userId']}";

$ORDER_HISTROY_QUERY =
" SELECT items.item_id,item_name,price,num,
DATE_FORMAT(order_date,'%H時%i分') AS time,
DATE_FORMAT(order_date,'%c月%e日') AS month_day,
DATE_FORMAT(order_date,'%c') AS month,
DATE_FORMAT(order_date,'%e') AS day
 FROM orders LEFT JOIN items ON orders.item_id = items.item_id
 WHERE user_id = {$_SESSION['userId']}
 ORDER BY order_date DESC, order_id ASC";
