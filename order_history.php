<?php
session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');

include "load_smarty.php";
include "urls.php";
include "functions.php";
include "qeries.php";

//ユーザーIDとユーザーネームを取得してアサインする
$userSession = isset($_SESSION["userId"]);
$userName = "";
if (isset($_SESSION["userName"])) {
    $userName = $_SESSION["userName"];
}
$smarty->assign('userSession', $userSession);
$smarty->assign('userName', $userName);

//未選択”０”のカテゴリリストを取得
$smarty->assign('categoryArray', getCategories(0));

//検索ボックスに入れた商品名を取得
$itemName = getInput('itemName');
$smarty->assign('itemName', $itemName);


$result = getOrderHistroy($ORDER_HISTROY_QUERY);

$itemTemp = array();
$orderResult = array();
$month_day = "";
$oldMonth_day = "";
$totalPrice = 0;

foreach ($result as $key => $value) {
    if ($month_day != $value['month_day']) {
        $month_day = $value['month_day'];

        if ($itemTemp) {
            $orderResult[] = array("month_day" => $oldMonth_day, "{$oldMonth_day}" => $itemTemp
            , "total" => $totalPrice);
        }

        //echo $month_day."<br>";
        //echo $value['item_name']."___________".$value['price']."<br>";
        $totalPrice = 0;
        $itemTemp = array();
        $itemTemp[] = array(
            "item_id" => $value['item_id']
            ,"item_name" => $value['item_name']
            ,"price" => $value['price']
            ,"num" => $value['num']
            ,"time" => $value['time']);
    } else {
        $itemTemp[] = array(
            "item_id" => $value['item_id']
            ,"item_name" => $value['item_name']
            ,"price" => $value['price']
            ,"num" => $value['num']
            ,"time" => $value['time']);
        //echo $value['item_name']."___________".$value['price']."<br>";
    }
    $totalPrice += $value['price'] * $value['num'];
    $oldMonth_day = $value['month_day'];
}
//一番最後が入らないので入れる
$orderResult[] = array( "month_day" => $oldMonth_day,"{$oldMonth_day}" => $itemTemp, "total" => $totalPrice);
//var_dump($orderResult);
//echo "<br><br><br>";
foreach ($orderResult as $key => $value) {
    //echo $value['month_day']."<br>";
    $date = $value['month_day'];
    foreach ($value["$date"] as $key2 => $value2) {
        //echo $value2['item_name']."_______price = ".$value2['price']."<br>";
    }
    //echo "<br>";
}
$smarty->assign('orderHistroy', $orderResult);
$smarty->display('order_histroy.tpl');
