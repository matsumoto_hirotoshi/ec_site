<?php
include "load_smarty.php";
include "functions.php";
include "urls.php";

session_start();
$nameError = "";
$addressError = "";
$passError = "";
$errors = array();
$name = "";
$mail = "";
$pass = "";
$hashPass = "";

if (isset($_SESSION["newName"]) && isset($_SESSION["newMail"])) {
    $smarty->assign('name', $_SESSION["newName"]);
    $smarty->assign('mail', $_SESSION["newMail"]);
    unset($_SESSION["newName"]);
    unset($_SESSION["newMail"]);
} else {
    $smarty->assign('name', $name);
    $smarty->assign('mail', $mail);
}


if (!empty($_POST)) {
    $name = filter_input(INPUT_POST, 'name');
    $mail = (string)filter_input(INPUT_POST, 'mail');
    $pass = filter_input(INPUT_POST, 'pass');
    $hashPass = password_hash($pass, PASSWORD_BCRYPT);

    // ユーザー名の入力チェック
    if (empty($name)) {
        $errors[] = "「ユーザー名」は必ず入力してください。";
    } else {
        $name = filter_input(INPUT_POST, 'name');
        $smarty->assign('name', $name);
    }
    // メールアドレスの入力チェック
    if (empty($mail)) {
        $errors[] = "「メールアドレス」は必ず入力してください。";
    } elseif (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        $errors[] = "正しいメールアドレスを入力してください。";
        $smarty->assign('mail', $mail);
    } elseif (login($mail) != false) {
        $errors[] = "すでに登録されているメールアドレスです。";
        $smarty->assign('mail', $mail);
    } else {
        $mail = filter_input(INPUT_POST, 'mail');
        $smarty->assign('mail', $mail);
    }
    // パスワードの入力チェック
    if (empty($pass)) {
        $errors[] = "「パスワード」は必ず入力してください。";
    }

    if (empty($errors)) {
        $_SESSION["newName"] = $name;
        $_SESSION["newMail"] = $mail;
        $_SESSION["newPass"] = $hashPass;
        header('Location: check.php');
    }
}

$smarty->assign('errors', $errors);
$smarty->display('register.tpl');
