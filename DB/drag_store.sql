-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2016 at 09:10 AM
-- Server version: 10.1.9-MariaDB-log
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `drag_store`
--
CREATE DATABASE IF NOT EXISTS `drag_store` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `drag_store`;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `brand` varchar(32) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand`, `manufacturer_id`) VALUES
(1, 'ネイチャーメイド', 1),
(2, 'インナーシグナル', 1),
(3, 'ULOS', 1),
(4, 'カロリーメイト', 1),
(5, '大塚製薬', 1),
(6, 'DHC', 2),
(7, 'FANCL', 3);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category`) VALUES
(1, 'ビタミン'),
(2, 'ミネラル'),
(3, 'コスメ'),
(4, '健康食品');

-- --------------------------------------------------------

--
-- Table structure for table `charts`
--

CREATE TABLE `charts` (
  `chart_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `num` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `charts`
--

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `destination_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_name` varchar(16) DEFAULT NULL,
  `postcode` varchar(8) DEFAULT NULL,
  `prefecture_id` int(11) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `phone_number` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `destinations`
--


-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(11) NOT NULL,
  `item_name` varchar(128) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `ingredients` varchar(1024) DEFAULT NULL,
  `information` varchar(4096) DEFAULT NULL,
  `image_path` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `category_id`, `manufacturer_id`, `brand_id`, `price`, `ingredients`, `information`, `image_path`) VALUES
(1, 'ビタミンC250', 1, 1, 1, 1300, '●たんぱく質:0.003g●脂質:0.01g', 'konnnitiha', '1-vitamin-C250-NatureMaid.jpg'),
(3, 'ビタミンC', 1, 2, 6, 629, '●ビタミンC 1,000mg（1,250％）、 ●ビタミンB2 2mg（180％）　', 'キレイと元気をつくる毎日の必需品。ビタミンCにその働きを助けるビタミンB2をプラスしました。', '2-vitamin-vitaminC-DHC.png'),
(4, 'アセロラ果実ビタミンC＆ポリフェノール', 1, 3, 7, 1738, '●ビタミンC:310mg（388%）、●ポリフェノール:10mg', '果実の中でもビタミンCの含有量が非常に高いアセロラ。中でも“早摘アセロラ”は、完熟アセロラの約1.5倍のビタミンCを含みます。「天然果実ビタミンC 早摘みアセロラ」は、契約農家で徹底管理された“早摘みアセロラ”を100％使用しています。', '3-vitamin-acerolaC-FANCL.jpg'),
(5, 'ビタミンBコンプレックス', 1, 1, 1, 561, '●ビタミンB1:15mg●ビタミンB2:12mg●ビタミンB6:10mg●ビタミンB12:15μg●ナイアシン:10mg●パントテン酸:10mg●葉酸:240μg●ビオチン:50μg', '不規則な生活の方に。エネルギーの産生に欠かせない8種類のビタミンB群を1日1粒で摂れます。美容を気にする方にもおすすめ。', '4-vitamin-Bcomplex-NatureMaid.jpg'),
(6, 'ビタミンB-MIX', 1, 2, 6, 600, '●ナイアシン40mg、●葉酸200μg、●ビオチン50μg、●ビタミンB12 20μg、●パントテン酸40mg、●ビタミンB1 40mg、●ビタミンB2 30mg、●ビタミンB6 30mg、●イノシトール50mg', 'ビタミンB群は、糖分やたんぱく質などの栄養素を代謝するときに、酵素を助けて補酵素になる必須ビタミン。脂質や糖質をエネルギーに変えるのに欠かせないことから、ダイエットにおすすめです。', '5-vitamin-Bcomplex-DHC.png'),
(7, 'ビタミンB群', 1, 3, 7, 1015, '●ビタミンB1：10mg（1000％）※、●ビタミンB2：40mg、●ビタミンB6：30mg、●ビタミンB12：6μg（300%）※、●ナイアシン：30mg（273％）※、●葉酸：400μg、●パントテン酸：30mg、●ビオチン：50μg（111％）※、●イノシトール：50mg', '水溶性で消耗しやすく、ストックできないビタミンB群。ファンケルの「ビタミンB群 ロングタイム製法」は、吸収が穏やかで体内で長時間機能するように工夫しました。', '6-vitamin-Bcomplex-FANCL.jpg'),
(8, 'ビタミンＥ200', 1, 1, 1, 1181, '●ビタミンE:134mg', '体内の脂質を酸化から守り、若々しい生活をサポートします。', '7-vitamin-E-NatureMaid.jpg'),
(9, 'ビタミンＥ', 1, 2, 6, 900, '●ビタミンE（d-α-トコフェロール)301.5mg', 'DHCの「ビタミンE」は、ビタミンEの中でもっとも活性の高い天然d-α-トコフェロールを1日あたり301.5mg配合。中高年期を健康に過ごしたい方におすすめのサプリメントです。', '8-vitamin-E-DHC.png'),
(10, 'カルシウム', 2, 1, 1, 811, '●カルシウム:300mg●ビタミンD:2.5μg', '牛乳や小魚が苦手な方に。日本人にもっとも不足しているミネラル。特に女性は生涯を通して、摂るべきです。', '11-mineral-cal-NatureMaid.jpg'),
(11, 'カルシウム・コーラル', 2, 2, 6, 300, '●カルシウム600mg、●ビタミンD（ビタミンD3）〈8IU〉0.2μg、', '「カルシウム[コーラル]」は、天然の造礁サンゴの粉末が主成分です。また、カルシウムの吸収を助けるビタミンD3やカゼインホスホペプチド(CPP)も配合。', '12-mineral-Cal-DHC.png'),
(12, 'カルシウム＆植物性ツイントース', 2, 3, 7, 483, '●カルシウム：350mg（50％）※、●マグネシウム：175mg（70％）※、●ビタミンD：2.5μg（50％）※、●ビタミンK：30μg、総イソフラボン：1.2mg（大豆イソフラボンアグリコン換算値：0.75mg）、●ビタミンP（ヘスぺリジン）：6mg、植物性ツイントース：500mg', 'ファンケルの「カルシウム＆植物性ツイントース」は、骨の健康に大切な成分と、カルシウムの吸収を助ける植物性ツイントースを配合し、健康で丈夫な毎日をサポートします。', '13-mineral-CalAnd-FANCL.jpg'),
(13, '亜鉛', 2, 1, 1, 664, '亜鉛:10mg', '味覚を正常に保つのに必要な栄養素です。皮膚や粘膜の健康維持を助ける栄養素です。タンパク質・核酸の代謝に関与して、健康の維持に役立つ栄養素です。', '14-mineral-Zinc-NatureMaid.jpg'),
(14, '亜鉛', 2, 2, 6, 267, '亜鉛15mg、クロム60μg、セレン50μg　', 'DHCの「亜鉛」は、規格基準を満たす栄養機能食品。さらに、健康値対策に役立つ“クロム”と若々しさをサポートする“セレン”も配合しました。食事が偏りがちな方や男性パワーにお悩みの方におすすめです。', '15-mineral-Zinc-DHC.png'),
(15, '亜鉛＆植物性ツイントース', 2, 3, 7, 864, '亜鉛：15mg', '食事の偏りによって不足しがちな必須ミネラルの亜鉛は、体の様々な機能に大切な成分。\r\nファンケルの「亜鉛＆植物性ツイントース」は、独自成分「植物性ツイントース」をはじめ、葉酸・ビタミンB2を配合し吸収にこだわりました。', '16-mineral-Zinc-FANCL.jpg'),
(16, 'マルチミネラル', 2, 1, 1, 785, '●ナトリウム:0～2mg●カルシウム:250mg●マグネシウム:125mg●亜鉛:6mg●鉄:4mg●銅:0.6mg●セレン:50μg●クロム:20μg', '体内で合成できない7種類のミネラルを１日1粒で手軽に補給できます。', '17-mineral-Multi-NatureMaid.jpg'),
(17, 'マルチミネラル', 2, 2, 6, 430, 'カルシウム250mg、鉄7.5mg、亜鉛6.0mg、銅0.6mg、マグネシウム125mg、セレン30.2μg、クロム28.3μg、マンガン1.5mg、ヨウ素50.8μg、モリブデン10.5μg', 'DHCの「マルチミネラル」は、栄養機能食品。カルシウム、マグネシウムのほか、銅、亜鉛、鉄、クロム、セレン、モリブデン、マンガン、ヨウ素といった10種類のミネラルをバランスよく配合しました。', '18-mineral-Multi-DHC.png'),
(18, 'マルチミネラル', 2, 3, 7, 915, 'カルシウム：350mg（50％）、鉄：3.75mg（50％）、亜鉛：3.5mg（50％）、マグネシウム：125mg（50％）、銅：0.3mg（50％）、ヨウ素：45μg（50％）、マンガン：1.75mg（50％）、セレン：11.5μg（50％）、クロム：15μg（50％）、モリブデン：8.5μg（50％）、植物性ツイントース：300mg', 'ファンケルの「マルチミネラル」は、必須ミネラル10種類すべての1日に必要な量※の50％をバランスよく補えます。さらに、ミネラルの吸収を助ける植物性ツイントースを配合しています。', '19-mineral-Multi-FANCL.jpg'),
(19, 'UL・OS薬用スカルプシャンプー', 3, 1, 3, 2592, 'シメン-5-オール、グリチルリチン酸ジカリウム', 'ウル・オス 薬用スカルプシャンプーは、頭皮と毛髪、毛穴を、やさしく、清潔に洗い上げ、フケやかゆみ、汗のにおいを防いで、すこやかに保ちます。', '21-cosme-shanpoo-otsuka.jpg'),
(20, 'DHC薬用ヘッドシャンプー', 3, 2, 6, 1750, 'ローズマリー、炎症を抑える甘草誘導体やアラントイン、皮脂分泌を調整するビタミンB類を配合', 'すぐれた洗浄力で清潔な髪と頭皮に整え、フケ・かゆみを抑制。さっぱりとした爽快な洗い上がりが心地よい薬用シャンプーです。', '22-cosme-shanpoo-DHC.jpg'),
(21, 'アミノシャンプー', 3, 3, 7, 1435, 'アルギニン ガーデニアタヒテンシス花エキス ユズ果実エキス セージ葉エキス アルテア根エキス タチジャコウソウエキス ローズマリー葉エキス オリーブ油', '必要なうるおいは残しながら、\r\nたっぷりの泡で汚れや不要な皮脂をしっかり落とす\r\n無添加のノンシリコンシャンプー。', '23-cosme-shanpoo-FANCL.jpg'),
(22, 'リジュブネイトローション', 3, 1, 2, 3000, 'アデノシン一リン酸二ナトリウム', '薬用有効成分エナジーシグナルAMPと、浸透型グルコースやヒアルロン酸など9種類の保湿成分配合。さらなるAMPをチャージすると共に、角層のすみずみまでたっぷりとうるおいを与えます。', '24-cosme-skinCare-otsuka.jpg'),
(23, 'DHC薬用ウィークスキンローション', 3, 2, 6, 2500, 'ポリグルタミン酸 植物性保湿成分酵母エキス、ビタミンA、D3', '植物由来のうるおい保護成分ポリグルタミン酸を配合した、デリケートな肌のためのローションです。肌に高いしっとり感を与えながら健康な肌に導き、しなやかな素肌に整えます。', '25-cosme-skinCare-DHC.jpg'),
(24, 'Bタイプ スキンローション', 3, 3, 7, 1620, 'ヒアルロン酸Ｎａ  ブナエキス  パセリエキス', 'しっとりした使用感のファンケル化粧液と、みずみずしくさっぱりした使用感のスキンローション。保湿成分が肌の内側をふっくらとうるおわせながら、皮膚本来の保湿能力を高めて肌をイキイキとすこやかに保ちます。', '26-cosme-skinCare-FANCL.jpg'),
(25, 'UL・OS日やけ止め25', 3, 1, 3, 756, 'アデノシンリン酸', '日にやけた小麦色の肌に健康イメージを重ねる人も多いはず。しかし紫外線による日やけが様々な肌トラブルの一因であるのも確か。\r\n“肌の健康”という視点から開発されたのがウル・オスの日やけ止め。', '27-cosme-sunShade-otsuka.jpg'),
(26, 'DHC UVプロテクション フェース ミルク', 3, 1, 6, 1150, '  オリーブバージンオイル、ヒアルロン酸、コラーゲン、スサビノリエキス  ', '  うるおってベタつかない！ スキンケア感覚の強力UVカットジェル\r\n ', '27-cosme-sunShade-otsuka.jpg'),
(28, 'サンガード20 デイリーUV ', 3, 3, 6, 1440, 'フトモモ葉エキス、スイートピー花エキス、サクシニルアテロコラーゲン、ヒアルロン酸Ｎａ', '紫外線をしっかり防いで肌を守るUVケア用クリーム。なめらかに伸びてベタつかず、うるおいはしっかりと守ります。', '29-cosme-sunShade-FANCL.jpg'),
(30, 'カロリーメイト', 4, 1, 4, 200, '食物繊維', 'カロリーメイトは、カラダに必要な栄養素をどこでもとれる忙しい現代人の食生活をバックアップするバランス栄養食です。', '31-helth-balanceFood-otsuka.jpg'),
(31, 'おからクッキー', 4, 2, 6, 372, 'おから、小麦', 'たっぷり食物繊維がおいしく摂れる！ 国産抹茶の風味が楽しめるおからクッキー', '32-helth-balanceFood-DHC.png'),
(32, '発芽米シリアルバー バランススタイル', 4, 3, 7, 1008, '大豆胚芽、小麦ブラン', '栄養豊富な発芽米を使用したバランス栄養食。食べごたえのある食感や美味しさをさらに追求し、フルーツやナッツ、大豆胚芽をプラスしました。', '33-helth-balanceFood-FANCL.jpg'),
(33, 'オロナミンＣドリンク', 4, 1, 5, 104, '●ビタミンB2:2.4mg ●ビタミンB6:6mg ●ナイアシン:12mg ●ビタミンC:220mg', '「オロナミンCドリンク」はビタミンCをはじめとする各種ビタミンが入った炭酸栄養ドリンクです。', '34-helth-drink-otsuka.jpg'),
(34, 'DHCコラーゲンビューティ7000プラス', 4, 2, 6, 2000, '低分子コラーゲンペプチド7，000mg', 'DHCコラーゲンビューティ7000プラスは、毎日つづけて飲むだけでキレイのピークがぐんぐん高まる、新処方のコラーゲンドリンクです。', '35-helth-drink-DHC.png'),
(41, '賢者の食卓ダブルサポート', 4, 1, 5, 1800, '難消化性デキストリン:5g', '食物繊維（難消化性デキストリン）の働きで糖分や脂肪の吸収を抑え、食後の血糖値や中性脂肪の上昇をおだやかにします。', '37-helth-diet-otsuka.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

CREATE TABLE `manufacturers` (
  `manufacturer_id` int(11) NOT NULL,
  `manufacturer` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`manufacturer_id`, `manufacturer`) VALUES
(1, '大塚製薬'),
(2, 'DHC'),
(3, 'ファンケル');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--
-- --------------------------------------------------------

--
-- Table structure for table `prefectures`
--

CREATE TABLE `prefectures` (
  `prefecture_id` int(11) NOT NULL,
  `prefecture` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prefectures`
--

INSERT INTO `prefectures` (`prefecture_id`, `prefecture`) VALUES
(1, '北海道'),
(2, '青森県'),
(3, '岩手県'),
(4, '宮城県'),
(5, '秋田県'),
(6, '山形県'),
(7, '福島県'),
(8, '茨城県'),
(9, '栃木県'),
(10, '群馬県'),
(11, '埼玉県'),
(12, '千葉県'),
(13, '東京都'),
(14, '神奈川県'),
(15, '新潟県'),
(16, '富山県'),
(17, '石川県'),
(18, '福井県'),
(19, '山梨県'),
(20, '長野県'),
(21, '岐阜県'),
(22, '静岡県'),
(23, '愛知県'),
(24, '三重県'),
(25, '滋賀県'),
(26, '京都府'),
(27, '大阪府'),
(28, '兵庫県'),
(29, '奈良県'),
(30, '和歌山県'),
(31, '鳥取県'),
(32, '島根県'),
(33, '岡山県'),
(34, '広島県'),
(35, '山口県'),
(36, '徳島県'),
(37, '香川県'),
(38, '愛媛県'),
(39, '高知県'),
(40, '福岡県'),
(41, '佐賀県'),
(42, '長崎県'),
(43, '熊本県'),
(44, '大分県'),
(45, '宮崎県'),
(46, '鹿児島県'),
(47, '沖縄県');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(32) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`),
  ADD KEY `brands_ibfk_1` (`manufacturer_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `charts`
--
ALTER TABLE `charts`
  ADD PRIMARY KEY (`chart_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`destination_id`),
  ADD KEY `address_id` (`prefecture_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `items_ibfk_1` (`category_id`),
  ADD KEY `items_ibfk_2` (`brand_id`),
  ADD KEY `items_ibfk_3` (`manufacturer_id`);

--
-- Indexes for table `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`manufacturer_id`),
  ADD KEY `manufacturer_id` (`manufacturer_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `prefectures`
--
ALTER TABLE `prefectures`
  ADD PRIMARY KEY (`prefecture_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `charts`
--
ALTER TABLE `charts`
  MODIFY `chart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `destination_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `prefectures`
--
ALTER TABLE `prefectures`
  MODIFY `prefecture_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_ibfk_1` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`manufacturer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `charts`
--
ALTER TABLE `charts`
  ADD CONSTRAINT `charts_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `charts_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `destinations`
--
ALTER TABLE `destinations`
  ADD CONSTRAINT `destinations_ibfk_2` FOREIGN KEY (`prefecture_id`) REFERENCES `prefectures` (`prefecture_id`),
  ADD CONSTRAINT `destinations_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_ibfk_3` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`manufacturer_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `items` (`item_id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
