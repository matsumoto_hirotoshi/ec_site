<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type"content="text/html; charset=UTF-8">
		<title>ec_site</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<body>
			<header>
				<div class="topForm">
					<form id="search_form" action="drag_store.php" method="post">
						<p><a href="{$top_page}"><img src="image/tengoku.png"></a>カテゴリ
							<select name="category" method="post">
								<option value="NULL">すべて</option>
								{foreach from=$categoryArray item=value key=key}
									<option value={$value}>{$key}</option>
								{/foreach}
							</select>

							キーワードで調べる<input type="text" size="12" name="itemName" value="{$itemName}"/>
							<input type="submit" name="submit" value="絞込検索">

							{if $userSession == true}
								ようこそ<a href="{$order_history_page}">{$userName}</a>さん
								<a href="{$shop_chart_page}">ショッピングカートへ</a>
								<a href="{$logout_page}">ログアウト</a>する
							{else}
								<a href="{$login_page}">ログイン</a>する
								<a href="{$register_page}">新規会員登録</a>
							{/if}
					</form>
				<div>
			</header>
			<div class="warapper">
				<div class="leftCont">
					<div class="userMenu">
						<ul style="list-style:none;">
							{if $userSession == true}
							<li><a href="{$shop_chart_page}">カートを見る</a></li>
							<li><a href="{$order_history_page}">購入履歴</a></li>
							{/if}
						</ul>
					</div>
					<div class="categoryMenu">
						<ul style="list-style:none;">
							<li>カテゴリ</li>
							{foreach from=$categoryArray item=value key=key}
	        					<li><a href="{$top_page}?category={$value}">{$key}</a></li>
	        				{/foreach}
	    				</ul>
					</div>
				</div>
				<div class="rightCont">
					{foreach from=$itemResult item=value key=key}
						<div class="itemLeft">
				     		<a href="{$item_page}?id={$value["item_id"]}">
				     			<img src= image/{$value["image_path"]} width="150" height="150" alt="画像"></a><br>
							<a href="{$item_page}?id={$value["item_id"]}">{$value["item_name"]}</a><br>
							<a href="{$top_page}?brand={$value["brand"]}">{$value["brand"]}</a><br>
							<span style="color:red;font-weight: bold ;">&yen; {$value["price"]}</span>
						</div>
			        {/foreach}
			    </div>
			</div>
		</body>
	</head>
<html>