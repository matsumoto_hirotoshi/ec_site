<!DOCTYPE html>
<html>
	<head>
	 <meta http-equiv="Content-Type" 
            content="text/html; charset=UTF-8">
		<title>ec_site</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<body>

		{if $result}
			<span style="font-size: 2.5em; margin-left: 10em;">登録済みの住所を選択する</span>
		{/if}

		<div class="registerAddressWarapper">
		{foreach from=$result item=value key=key}
			<div class="registeredAddressBox">
					<div class="registeredAddressTableRow">
						<div>宛先</div>
						<div>{$value['post_name']}</div>
					</div>
					<div class="registeredAddressTableRow">
						<div>郵便番号</div>
						<div>{$value['postcode']}</div>
					</div>
					<div class="registeredAddressTableRow">
						<div>住所</div>
						<div>{$value['prefecture']}</div>
					</div>
					<div class="registeredAddressTableRow">
						<div>電話番号</div>
						<div>{$value['phone_number']}</div>
					</div>
					<div class="registeredAddressButton">
						<a href="{$order_confirm_page}?address_id={$value['destination_id']}"><input type="button" value="この住所に送る"></a>
					</div>
			</div>
		{/foreach}
		<br clear="left">
		<form action="{$register_address_page}" method="post">
			<span style="font-size: 2.5em; margin-left: 10em;">新しい住所を登録する</span>
			<table class="registerAddresTable">
				<tbody>
				<tr>
					<td>氏名</td>
					<td><input type="text" name="name"></td>
				</tr>
				<tr>
					<td>郵便番号</td>
					<td>
						<input type="text" size="2" name="postcode1">
						 - <input type="text" size="6" name="postcode2">
					</td>
				</tr>
				<tr>
					<td>住所</td>
					<td>
						都道府県<br>
						<select name="prefecture_id">
							{foreach from=$prefectures item=value key=key}
								<option value={$value}>{$key}</option>
							{/foreach}
						</select><br>
						市区都<br>
						<input type="text" name="address1"><br>
						町名・番地・建物名<br>
						<input type="text" name="address2"><br>
					</td>
				</tr>
				<tr>
					<td>
						電話番号
					</td>
					<td>
						<input type="text" size="2" name="phoneNum1">
						 - <input type="text" size="6" name="phoneNum2">
						 - <input type="text" size="6" name="phoneNum3">
					</td>
				</tr>
				</tbody>
			</table>
			<div align="center"><input type="submit" name="submit" value="この住所を登録する"></div><br>
		</form>
		</body>
	</head>
<html>