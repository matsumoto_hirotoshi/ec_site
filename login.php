<?php
include "load_smarty.php";
include "functions.php";
include "urls.php";

session_start();

$nameError = "";
$addressError = "";
$passError = "";

if (isset($_POST) && !empty($_POST)) {
    $result = login(filter_input(INPUT_POST, 'mail'));

    if (password_verify(filter_input(INPUT_POST, 'pass'), $result['password'])) {
        $_SESSION["login"] = "ok";
        $_SESSION["userId"] = $result["user_id"];
        $_SESSION["userName"] = $result["user_name"];

        header('Location: drag_store.php');
          exit();
    } else {
        echo "メールアドレスまたはパスワードが間違っています";
    }
}

$smarty->display('login.tpl');
