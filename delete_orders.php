<?php

session_start();
$data = array();
$itemId = filter_input(INPUT_POST, 'itemId');
include "functions.php";

$pdo = initDB();
$sql = " DELETE FROM charts WHERE user_id = :userId AND item_id =:itemId";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(":userId", $_SESSION['userId'], PDO::PARAM_INT);
$stmt->bindParam(":itemId", $itemId, PDO::PARAM_INT);

$stmt->execute();

$sql = " DELETE FROM charts WHERE user_id = {$_SESSION['userId']} AND item_id = {$itemId}";
$directory_path = "./csv";

if (!file_exists($directory_path)) {
    if (!mkdir($directory_path, 0777)) {
        echo "フォルダの作成に失敗しました";
    }
}

$fp = fopen("./csv/queryLog.csv", "a");
$temp = array($sql);
fputcsv($fp, $temp);
fclose($fp);
    
header('Content-Type: application/json; charset=utf-8');
echo json_encode($data);
