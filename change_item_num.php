<?php

session_start();
include "functions.php";

$itemNum = filter_input(INPUT_POST, 'itemNum');
$itemId = filter_input(INPUT_POST, 'itemId');

$pdo = initDB();
$sql = "UPDATE charts SET num = :num WHERE user_id = :userId AND item_id =:itemId ";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(":num", $itemNum, PDO::PARAM_INT);
$stmt->bindParam(":userId", $_SESSION['userId'], PDO::PARAM_INT);
$stmt->bindParam(":itemId", $itemId, PDO::PARAM_INT);

$stmt->execute();

$directory_path = "./csv";

if (!file_exists($directory_path)) {
    if (!mkdir($directory_path, 0777)) {
        echo "フォルダの作成に失敗しました";
    }
}

$fp = fopen("./csv/queryLogChangeItemNum.csv", "w");
$temp = array($itemNum, $itemId, date("Y/m/d", time()));
fputcsv($fp, $temp);
fclose($fp);

//mysql_close($conn);

header('Content-Type: application/json; charset=utf-8');
echo json_encode($data);
