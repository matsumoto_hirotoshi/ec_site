<?PHP
session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');

include "load_smarty.php";
include "urls.php";
include "functions.php";
include "qeries.php";

$addresId = filter_input(INPUT_GET, 'address_id');
$sql = "SELECT * FROM destinations WHERE destination_id = {$addresId}";

$destination   = getDBResult($sql)->fetch(PDO::FETCH_ASSOC);
$smarty->assign('destination', $destination);


$sql = "SELECT *,(SELECT SUM(price * num) 
                    FROM charts LEFT JOIN items ON charts.item_id = items.item_id 
                    WHERE user_id = {$_SESSION['userId']}) AS chartTotalPrice 
                    FROM charts LEFT JOIN items ON charts.item_id = items.item_id 
                    WHERE user_id = {$_SESSION['userId']}";
$charts = getDBResult($sql)->fetchAll();
$smarty->assign('charts', $charts);
$smarty->assign('chartTotalPrice', $charts[0]['chartTotalPrice']);

$smarty->display('order_confirm.tpl');
