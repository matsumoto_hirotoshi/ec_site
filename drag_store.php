<?php
session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');
include "load_smarty.php";
include "functions.php";
include "urls.php";

//ユーザーIDとユーザーネームを取得してアサインする
$userSession = isset($_SESSION["userId"]);
$userName = "";
if (isset($_SESSION["userName"])) {
    $userName = $_SESSION["userName"];
}

$smarty->assign('userSession', $userSession);
$smarty->assign('userName', $userName);

//GETとPOSTを取得してDBにクエリを投げて取得する
$category = "";
$brand = "";
$itemName = "";
$category = getInput('category');
$brand = getInput('brand');
$itemName = getInput('itemName');
$result = getItems($category, $brand, $itemName);

//選択済みのカテゴリリストを取得
$selected = $category;
$smarty->assign('categoryArray', getCategories($selected));

//検索ボックスに入れた商品名を取得
$smarty->assign('itemName', $itemName);
$smarty->assign('itemResult', $result);

$smarty->display('drag_store.tpl');
