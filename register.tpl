<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type"content="text/html; charset=UTF-8">
		<title>ec_site</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<body>
			<form action="register.php" method="post" enctype="multipart/form-data">
				{foreach from=$errors item=error}
					<ul>
					<li>{$error}</li>
					</ul>
				{/foreach}
				<dl>
					<dt><font color="red">【必須】</font>ユーザー名</dt>
					<dd>
					  <input type="text" name="name" size="35" maxlength="255" value="{$name}">
					</dd>
					<dt><font color="red">【必須】</font>メールアドレス</dt>
					<dd>
					  <input type="text" name="mail" size="35" maxlength="255" value="{$mail}">
					</dd>
					<dt><font color="red">【必須】</font>パスワード</dt>
					<dd>
					  <input type="password" name="pass" size="10" maxlength="20">
					</dd>
				</dl>
				<div>
					<input type="submit" name="confirm" value="入力内容を確認">
					<p>
						<a href="{$top_page}"><input type="button" value="トップページに戻る">
					</p>
				</div>
				
			</form>
		</body>
	</head>
<html>