<?php
function initDB()
{
    try {
            $dsn = '';
            $user = '';
            $password = '';
            $pdo = new PDO(
                $dsn,
                $user,
                $password,
                array(PDO::ATTR_EMULATE_PREPARES => false)
            );
    } catch (PDOException $e) {
        //exit('データベース接続失敗。'.$e->getMessage());
        echo mb_convert_encoding($e->getMessage(), 'utf-8', 'sjis');
    }
    return $pdo;
}
function getDBResult($sql)
{
    $pdo = initDB();
    $stmt = $pdo->query($sql);
    if (!$stmt) {
        echo $sql."<br>";
        print_r($pdo->errorInfo());
    }
    return $stmt;
}
function getCategories($selected)
{
    
    $pdo = initDB();

    $sql = "SELECT * FROM categories ORDER BY categories.category_id ASC";
    $stmt = getDBResult($sql);

    $categoryArray;
    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
        if ($selected == $result['category_id']) {
            $categoryArray["{$result['category']}"] = "\"".$result['category_id']."\" selected";
        } else {
            $categoryArray["{$result['category']}"] = $result['category_id'];
        }
    }
        return $categoryArray;
}

function getItem()
{

    $id = filter_input(INPUT_GET, 'id');
    $sql = " SELECT * FROM items LEFT JOIN brands ON items.brand_id = brands.brand_id WHERE item_id = {$id}";

    $resultArray = array();

    $stmt = getDBResult($sql);
    
    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $resultArray[] = array(
            'item_id' => $result['item_id'],
            'item_name' => $result["item_name"],
            'category' => $result["category_id"],
            'ingredients' => $result["ingredients"],
            'information' => $result["information"],
            'brand' => $result["brand"],
            'price' => $result["price"],
            'image_path' => $result["image_path"]);
    }
    return $resultArray;
}
function getItems($getCategory, $getBrand, $postItemName)
{
    $getBrand = $getBrand;
    $sql = " SELECT * FROM items LEFT JOIN brands ON items.brand_id = brands.brand_id ";
    if ($getCategory == "NULL") {
        $sql.="WHERE item_id ";
    } elseif ($getCategory) {
        $sql.="WHERE category_id = '{$getCategory}' ";
    } elseif ($getBrand) {
        $sql.="WHERE brand = '{$getBrand}'";
    } else {
        $sql.="WHERE item_id ";
    }
    
    if ($postItemName) {
        $sql.="AND item_name LIKE '%{$postItemName}%'";
    }

    $stmt = getDBResult($sql);

    return $stmt -> fetchAll();
}
    
function getInput($inputName)
{
    if (filter_input(INPUT_GET, $inputName)) {
        return filter_input(INPUT_GET, $inputName);
    } elseif (filter_input(INPUT_POST, $inputName)) {
        return filter_input(INPUT_POST, $inputName);
    } else {
        return "";
    }
}

function updateChart($getNum)
{
    if ($getNum) {
        $pdo = initDB();
        $sql = "SELECT * FROM charts WHERE user_id = {$_SESSION['userId']} AND item_id = {$_SESSION["select_item_id"]}";
        $stmt = $pdo->query($sql);

        if ($stmt->fetchAll()) {
            $sql = "UPDATE charts SET num = num + {$getNum} WHERE user_id
                = {$_SESSION["userId"]} AND item_id = '{$_SESSION["select_item_id"]}'";
            $stmt = $pdo->query($sql);
        } else {
            $sql = "INSERT INTO charts(chart_id, user_id, item_id, num) 
                VALUES('', '{$_SESSION["userId"]}', '{$_SESSION["select_item_id"]}', '{$getNum}')";
            $stmt = $pdo->query($sql);
        }
    }
}
function getChartItemsAndTotalPrice($userId, &$chartTotalPrice)
{
    $sql = " SELECT * FROM charts LEFT JOIN items ON charts.item_id = items.item_id WHERE user_id = {$userId}";
    $pdo = initDB();
    $stmt = $pdo->query($sql);

    $total = 0;

    $resultArray = array();
    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $numberElems = "";
        $limit = 10 - substr($result['num'], -1, 1);
        $limit += 10 + 1;

        for ($i = 1; $i < $result['num'] + $limit; $i++) {
            if ($result['num'] == $i) {
                $numberElems.="<option value=\"{$i}\" selected>{$i}</option>";
            } else {
                $numberElems.="<option value=\"{$i}\">{$i}</option>";
            }
        }

        $resultArray[] = array(
            'item_id' => $result['item_id'],
            'item_name' => $result["item_name"],
            'price' => $result["price"],
            'itemTotalPrice' => $result["price"] * $result['num'],
            'numberElem' => $numberElems);

        $total += $result["price"] * $result['num'];
    }
    
    $chartTotalPrice = $total;

    return $resultArray;
}
function getDestination($sql)
{
    $pdo = initDB();
    //$stmt = $pdo->query($sql);
    $stmt = getDBResult($sql);

    return $stmt -> fetch(PDO::FETCH_ASSOC);
}
function getDestinations($sql)
{
    $pdo = initDB();
    //$stmt = $pdo->query($sql);
    $stmt = getDBResult($sql);

    return $stmt -> fetchAll();
}
function getOrderHistroy($sql)
{
    $pdo = initDB();
    $stmt = getDBResult($sql);

    return $stmt -> fetchAll();
}
// 渡されたメールアドレスがDBに存在しているかのチェック存在していたらユーザー情報を返す。
// 新規登録する際にメールアドレスがすでに登録されているかのチェックにも使用する。
function login($mail)
{
    $pdo = initDB();

    $sql = 'SELECT * FROM users WHERE email = ?;';
    $sth = $pdo->prepare($sql);
    if (!$sth) {
        echo $sql."<br>";
        print_r($pdo->errorInfo());
    }
    $sth->bindParam(1, $mail, PDO::PARAM_STR);
    $sth->execute();

    return $sth->fetch(PDO::FETCH_ASSOC);
}
function getPrefectures()
{
    $pdo = initDB();
    $sql = "SELECT * FROM prefectures WHERE 1";
    $stmt = getDBResult($sql);
    
    $prefectures;
    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $prefectures["{$result['prefecture']}"] = $result['prefecture_id'];
    }
    
    return $prefectures;
}
