<?php
include "functions.php";
session_start();
echo filter_input(INPUT_POST, 'prefecture_id');
$name = filter_input(INPUT_POST, 'name');
$postcode  = filter_input(INPUT_POST, 'postcode1')."-";
$postcode .= filter_input(INPUT_POST, 'postcode2');
$prefecture_id   = filter_input(INPUT_POST, 'prefecture_id');
$address  = filter_input(INPUT_POST, 'address1');
$address  .= filter_input(INPUT_POST, 'address2');
$phoneNum  =  filter_input(INPUT_POST, 'phoneNum1')."-";
$phoneNum .= filter_input(INPUT_POST, 'phoneNum2')."-";
$phoneNum .= filter_input(INPUT_POST, 'phoneNum3');

$pdo = initDB();

// $stmt = $pdo -> prepare(
// "INSERT INTO destinations(address_id, user_id, post_name, postcode, address, phone_number)
// VALUES (:address_id, :user_id, :post_name, :postcode, :address, :phone_number)");
$stmt = $pdo->prepare(
    "INSERT INTO destinations(destination_id, user_id, post_name, postcode, prefecture_id, address, phone_number) 
    VALUES ('', :user_id, :post_name, :postcode, :prefecture_id, :address, :phone_number)"
);
//$stmt->bindValue(':destination_id', '', PDO::PARAM_INT);
$stmt->bindValue(':user_id', $_SESSION['userId'], PDO::PARAM_INT);
$stmt->bindParam(':post_name', $name, PDO::PARAM_STR);
$stmt->bindParam(':postcode', $postcode, PDO::PARAM_STR);
$stmt->bindValue(':prefecture_id', $prefecture_id, PDO::PARAM_INT);
$stmt->bindParam(':address', $address, PDO::PARAM_STR);
$stmt->bindValue(':phone_number', $phoneNum, PDO::PARAM_STR);

$stmt->execute();
$pdo->errorInfo();
// if(!$stmt){
// print_r( $pdo->errorInfo());
// }
header('Location: street_address.php');
