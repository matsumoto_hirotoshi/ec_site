<?PHP
session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');

include "load_smarty.php";
include "urls.php";
include "functions.php";

//ユーザーIDとユーザーネームを取得してアサインする
$userSession = isset($_SESSION["userId"]);
$userName = "";
if (isset($_SESSION["userName"])) {
    $userName = $_SESSION["userName"];
}
$smarty->assign('userSession', $userSession);
$smarty->assign('userName', $userName);

//未選択”０”のカテゴリリストを取得
$smarty->assign('categoryArray', getCategories(0));

//検索ボックスに入れた商品名を取得
$itemName = getInput('itemName');
$smarty->assign('itemName', $itemName);

if (isset($_SESSION["userId"])) {
    if (isset($_SESSION["select_item_id"]) && !empty($_SESSION["select_item_id"])) {
        if (filter_input(INPUT_GET, 'num')) {
            updateChart(filter_input(INPUT_GET, 'num'));
            unset($_SESSION["select_item_num"]);
        }
        unset($_SESSION["select_item_id"]);
    }
    
    $itemResult = getChartItemsAndTotalPrice($_SESSION["userId"], $chartTotalPrice);
    $smarty->assign('itemResult', $itemResult);
    $smarty->assign('total', $chartTotalPrice);

    $smarty->display('shop_chart.tpl');
} else {
    echo "ログインしてください";
}
