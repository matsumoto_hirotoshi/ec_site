<?php

session_start();
header('Expires:-1');
header('Cache-Control:');
header('Pragma:');

include "load_smarty.php";
include "urls.php";
include "functions.php";


//ユーザーIDとユーザーネームを取得してアサインする
$userSession = isset($_SESSION["userId"]);
$userName = "";
if (isset($_SESSION["userName"])) {
    $userName = $_SESSION["userName"];
}
$smarty->assign('userSession', $userSession);
$smarty->assign('userName', $userName);

//未選択”０”のカテゴリリストを取得
$smarty->assign('categoryArray', getCategories(0));

//検索ボックスに入れた商品名を取得
$itemName = getInput('itemName');
$smarty->assign('itemName', $itemName);

//itemIdをセット
$_SESSION['select_item_id'] = filter_input(INPUT_GET, 'id');

//DBから商品データを一行取得
$result = getItem();
$price = $result[0]['price'];
$smarty->assign('itemResult', $result);
$smarty->assign('price', $price);

$smarty->display('item_page.tpl');
